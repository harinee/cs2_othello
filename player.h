#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <algorithm>

using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    void setTestingMinimax(bool b);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
private:
    Side mySide;
    Side oppSide;
    Board *board;
    int depth;
    Move *minimaxMove;
    
    std::vector<Move *> *findPossibleMoves(Board *b, Side s);
    void clearVector(std::vector<Move *> *vec);
    int minimax(int depth, Side s, Board *b, Move *m, int alpha, int beta);
    int findLargestCaptureWithCornerBias(std::vector<Move *> *possibleMoves);
    int findScoreMultiplier(Move *m);
    bool isAdjacent(Move *m);
    Move *simpleHeuristic();
    int getScoreOfBoard(Board *b, Move *m);
    int minimax(int d, Side s, Board *b);
    Move *doMinimax();
    int findScoreDivisor(Move *m);

    
};

#endif
