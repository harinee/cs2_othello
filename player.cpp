#include "player.h"
#include <cstdio>
#include <cmath>

//first modification

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    depth = 5;
    board = new Board();
    mySide = side;
    oppSide = (mySide ==  WHITE) ? BLACK : WHITE;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

void Player::setTestingMinimax(bool b) {
    testingMinimax = b;
    char boardData[64] = {
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', 'b', ' ', ' ', ' ', ' ', ' ', ' ', 
        'b', 'w', 'b', 'b', 'b', 'b', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '
    };
    board->setBoard(boardData);
}
void Player::clearVector(std::vector<Move *> *vec) {
    for(int i = 0; i < (int)vec->size(); i++)
        delete vec->at(i);
}

std::vector<Move *> *Player::findPossibleMoves(Board *b, Side s) {
    std::vector<Move *> *vec = new std::vector<Move *>();
    for(int i = 0; i < 8; i++) {
        for(int j = 0; j < 8; j++) {
            Move *m = new Move(i, j);
            if(b->checkMove(m, s))
                vec->push_back(m);
            else
                delete m;
        }
    }
    return vec;

}
/* A simply heuristic that analyzes avalable moves
 * based on the weights of corners, edges, and 
 * squares granting access to edges/corners.
 *
 * return index of the move with the largest
 * possible capture.
 */
int Player::findLargestCaptureWithCornerBias(std::vector<Move *> *possibleMoves) {

    int bestIndex = 0;
    int bestScore = -500;
    for(int i = 0; i < (int) possibleMoves->size(); i++){
        Board *b = board->copy();
        b->doMove(possibleMoves->at(i), mySide);
        int score = getScoreOfBoard(b, possibleMoves->at(i));
        if(score > bestScore) {
            bestScore = score;
            bestIndex = i;
        }
        delete b;     
    }
    return bestIndex;
}

int Player::findScoreMultiplier(Move *m) {
    int x = m->getX();
    int y = m->getY();

    bool isCorner = (x == 0 || x == 7) && (y == 0 || y == 7);
    bool isEdge = (x == 0 || x == 7) || (y == 0 || y == 7);
    if(isCorner)
        return 10;
    if(isAdjacent(m))
        return 0;
    if(isEdge)
        return 4;

    
    return 3;    

    
}

bool Player::isAdjacent(Move *m) {
    if((m->x == 1 || m->x == 6) && (m->y == 1 || m->y == 6))
        return true;
    
    return false;
}
int Player::findScoreDivisor(Move *m)
{
    Board *b = board->copy();
    b->doMove(m, mySide);
    std::vector<Move *> *vec = findPossibleMoves(b, oppSide);
    int ret = vec->size();
    delete vec;
    delete b;
    return ret;
}
int Player::getScoreOfBoard(Board *b, Move *m) {
    int numOfMyTiles = (mySide == BLACK) ? b->countBlack() : b->countWhite();
    int numOfOppTiles = (mySide == BLACK) ? b->countWhite() : b->countBlack();

    int score = (numOfMyTiles - numOfOppTiles);
    if(testingMinimax)
        return score;
        
    int mult = findScoreMultiplier(m);
    score *= mult;
    int divisor = findScoreDivisor(m);
    score = (int)(((double)(2 * score))/((double)(divisor)));
    return score;
    
}

Move *Player::simpleHeuristic() {
    std::vector<Move *> *possibleMoves = findPossibleMoves(board, mySide);
    int largestCapture = findLargestCaptureWithCornerBias(possibleMoves);
    Move *bestMove = possibleMoves->at(largestCapture);

    Move *ret = new Move(bestMove->x, bestMove->y);
    
    clearVector(possibleMoves);
    delete possibleMoves; 
    return ret;
}

int Player::minimax(int d, Side s, Board *b, Move *m, int alpha, int beta) {
    int al = alpha;
    int be = beta;
    if(b->isDone())
        return -1000;
    if(d == 0)
    {
        if(s == mySide) {
            int score = getScoreOfBoard(b, m);
            if(score > al)
                al = score;
            return score;
        }
        else {
            int score = getScoreOfBoard(b, m);
            if(score < be)
                be = score;
            return score;
        }
    }

    std::vector<Move *> *possibleMoves = findPossibleMoves(b, mySide);
    if(s == mySide) {
        int idxOfTop = 0;
        int topScore = -500;
        for(int i = 0; i < (int) possibleMoves->size(); i++) {
            Board *boardWithMoveAdded = b->copy();
            boardWithMoveAdded->doMove(possibleMoves->at(i), mySide);
            int score;
            if(d != 1) {
                score = minimax(d - 1, oppSide, boardWithMoveAdded, possibleMoves->at(i), al, be);
            }
            else {
                score = minimax(d - 1, oppSide, b, possibleMoves->at(i), al, be);
            }
            if(score > topScore) {
                idxOfTop = i;
                topScore = score;
            }
            if(al== be)
                break;
            delete boardWithMoveAdded;
        }
        if(d == depth)
            minimaxMove = new Move((possibleMoves->at(idxOfTop))->x, (possibleMoves->at(idxOfTop))->y); 
        clearVector(possibleMoves);
        delete possibleMoves;
        
        return topScore;
    }
    else {
        int topScore = +500;
        for(int i = 0; i < (int) possibleMoves->size(); i++) {
            Board *boardWithMoveAdded = b->copy();
            boardWithMoveAdded->doMove(possibleMoves->at(i), oppSide);
            int score;        
            if(d != 1) {
                score = minimax(d - 1, mySide, boardWithMoveAdded, possibleMoves->at(i), al, be);
            }
            else {
                score = minimax(d - 1, mySide, b, possibleMoves->at(i), al, be);
            }
            if(score < topScore)
                topScore = score;
            delete boardWithMoveAdded;
            if(be == al)
                break;
        }
        clearVector(possibleMoves);
        delete possibleMoves;
        return topScore;
    }
}

Move *Player::doMinimax() {

    int topScore = minimax(depth, mySide, board, NULL, -500, 500);
    topScore = 0;
    
    return minimaxMove;
}
/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    board->doMove(opponentsMove, oppSide);
    if(board->hasMoves(mySide) == false)
        return NULL;

    Move *m = doMinimax();
    //Move *m = simpleHeuristic();
    
    board->doMove(m, mySide);

    return m;
    
}
