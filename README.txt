In order to make my AI tournament worthy, I felt that the heuristic used
to score each of the outcomes of the minimax tree needed to be more 
representative of the position of the board. No matter how efficient the 
minimax search is, using alpha-beta pruning, if the heuristic doesn't
accureately represent the final board possition, it will mislead the AI.

Therefore, I updated the AI to also take into account other factors such
as the amount of moves my opponenent has. By dividing the score by this number,
I gave more weight to moves that limited my opponenent. Additionally, I looked
at the games lost to ConstantTimePlayer and noticed that I lost control of the
corners in most instances, leading to a crushing defeat. I modified the heuristic
to give more weight to the corners while giving a larger negative weight to the 
adjacent scores.

Finally, I implemented the alpha-beta pruning system to optimize my minimax tree.
